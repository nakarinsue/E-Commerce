public interface Closable {
    void close();
}
