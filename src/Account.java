public abstract class Account implements Closable{
    private Long id;
    private String userName;
    private String password;
    private String aacountName;
    private Status status;
    private Long balance;

    public Account(Long id, String userName, String password, String aacountName, Long balance) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.aacountName = aacountName;
        this.status = Status.NORMAL;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAacountName() {
        return aacountName;
    }

    public void setAacountName(String aacountName) {
        this.aacountName = aacountName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public void close() {
        this.status = Status.CLOSED;
    }

}
