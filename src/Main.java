import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
public class Main {

    public static final long tranferFee = 20L;

    public static void main(String[] args) {

        SavingAccount account1 = new SavingAccount(1L,
                "Nontawat", "1234",
                "Nontawat Wuttikam",1000000L);
        FixedDepositAccount account2 = new FixedDepositAccount(2L,
                "Nakarin", "1234",
                "Nakarin XXX",1000000L);
        SavingAccount account3 = new SavingAccount(3L,
                "Tanadon", "1234",
                "Tanadon Pornpipatanajira",1000000L);

        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);

        System.out.println("Program started : E-COMMERCE ...");

        Scanner scan = new Scanner(System.in);
        boolean exit = false;
        while (!exit) {
            System.out.println("Welcome to login screen : ");
            System.out.println("1. Login 2. Exit : ");
            Integer option = scan.nextInt();
            switch (option) {
                case 1 : {/*login*/
                    System.out.println("Please Enter your username : ");
                    String username = scan.next();
                    System.out.println("Please Enter your password : ");
                    String password = scan.next();

                    boolean loginSuccess = false;
                    long currentId = 0L;
                    for (Account account : accounts) {
                        if (username.equals(account.getUserName()))
                            if (password.equals(account.getPassword()))
                                if (account.getStatus() == Status.NORMAL) {
                                    loginSuccess = true;
                                    currentId = account.getId();
                                }
                    }

                    if (loginSuccess) {
                        //Menu
                        boolean menuClose = false;
                        while (!menuClose) {
                            System.out.println("----Menu----");
                            System.out.println("1. Withdraw");
                            System.out.println("2. Deposit");
                            System.out.println("3. Tranfer");
                            System.out.println("4. Terminate Account");
                            System.out.println("5. Exit menu");
                            System.out.println("Please choose an option : ");

                            Integer menuOption = scan.nextInt();

                            switch (menuOption) {

                                case 1: /*withdraw*/ {
                                    System.out.println("How much do you want to withdraw : ");
                                    long amount = scan.nextLong();
                                    long balance = 0L;
                                    for (Account account : accounts) {
                                        if (account.getId() == currentId) {
                                            account.setBalance(account.getBalance() - amount);
                                            balance = account.getBalance();
                                        }
                                    }
                                    System.out.println("Withdrawed : " + String.valueOf(amount) + " Remain : " + String.valueOf(balance));
                                    break;
                                }

                                case 2: /*Deposit*/ {
                                    System.out.println("How much do you want to deposit : ");
                                    long amount = scan.nextLong();
                                    long balance = 0L;
                                    for (Account account : accounts) {
                                        if (account.getId() == currentId) {
                                            account.setBalance(account.getBalance() + amount);
                                            balance = account.getBalance();
                                        }
                                    }
                                    System.out.println("Deposit : " + String.valueOf(amount) + " Remain : " + String.valueOf(balance));
                                    break;
                                }

                                case 3: /*Transfer*/ {
                                    System.out.println("Please enter the destination bank ID : ");
                                    long destinationId = scan.nextLong();
                                    System.out.println("How much do you want to transfer : ");
                                    long amount = scan.nextLong();
                                    for (Account account : accounts) {
                                        if (account.getId().equals(currentId)) {
                                            account.setBalance(account.getBalance() - amount);
                                            System.out.println("Transfer : " + String.valueOf(amount) + " Remain : " + String.valueOf(account.getBalance()));
                                        }
                                        if (account.getId().equals(destinationId)) {
                                            long money = account.getBalance() + amount - tranferFee;
                                            account.setBalance(money);
                                            System.out.println("Destination Balance : " + String.valueOf(money));
                                        }
                                    }
                                    break;
                                }

                                case 4: /*Terminate Account*/ {
                                    for (Account account : accounts) {
                                        if (account.getId() == currentId)
                                            account.setStatus(Status.CLOSED);
                                            menuClose = true;
                                    }
                                    break;
                                }

                                case 5: menuClose = true; break;

                            }
                        }
                    }
                    else System.out.println("Your information is incorrect!");
                    break;
                }
                case 2 : exit = true; break;
            }

        }

    }

}
